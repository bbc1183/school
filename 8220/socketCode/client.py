import socket
import random
import sys
import time

try:
    #CREATE THE CONNECTION
    clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientsocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
    print('Failed to create socket')
    sys.exit()

clientsocket1.connect(('localhost', 5002))
clientsocket2.connect(('localhost', 5003))
x=1
while x < 101:
    message = 'message: '+str(x)
    print("sending "+str(message))
    if(random.random() < 0.5):
        clientsocket1.send(bytes(message, 'UTF-8'))
        clientsocket1.settimeout(0.00002)
        try:
            reply = clientsocket1.recv(100)
            reply = str(reply,'UTF-8')
            print("receiver received ", reply)
        except socket.timeout:
            print("timed out")
            continue
    else:
        clientsocket2.send(bytes(message, 'UTF-8'))
        clientsocket2.settimeout(0.02)
        try:
            reply = clientsocket2.recv(100)
            reply = str(reply,'UTF-8')
            print("receiver received ",reply)
        except socket.timeout:
            print("timed out")
            continue
    x+=1
