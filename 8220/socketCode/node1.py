#!/usr/bin/env python

import socket
from threading import Thread
from socketserver import ThreadingMixIn
import queue
import time

class ClientThread(Thread):

    def __init__(self,ip,port, conn):
        Thread.__init__(self)
        self.ip = ip
        self.conn = conn
        self.port = port
        self.q = queue.Queue()
        #print ("[+] New thread started for "+ip+":"+str(port))



    def run(self):
        while True:
            data = conn.recv(2048)
            data = str(data,'UTF-8')
            self.q.put(data)
            if not data:
                break
            else:
                try:
                    self.conn.send(bytes(str(data), 'UTF-8'))
                except socket.error:
                    print("error sending response")

                print ("Node 1 received ", data)

                try:
                    #CREATE THE CONNECTION
                    clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    #clientsocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                except socket.error:
                    print('Failed to create socket')
                    break


                clientsocket1.connect(('localhost', 5001))
                #time.sleep(0.2)
                clientsocket1.send(bytes(str(self.q.queue[0]), 'UTF-8'))
                self.q.get()
                clientsocket1.settimeout(0.00002)

                try:
                    reply = clientsocket1.recv(100)
                    if reply:
                        reply = str(reply,'UTF-8')
                        print("receiver received ", reply)


                except socket.timeout:
                    print("timed out")
                    continue



TCP_IP = 'localhost'
TCP_PORT = 5002
BUFFER_SIZE = 20  # Normally 1024, but we want fast response


tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpsock.listen(4)
    #print( "Waiting for incoming connections...")
    (conn, (ip,port)) = tcpsock.accept()
    newthread = ClientThread(ip,port, conn)
    newthread.start()
    threads.append(newthread)

for t in threads:
    t.join()
