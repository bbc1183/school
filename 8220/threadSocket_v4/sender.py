import socket
import random
import sys
import time

import threading



try:
    #CREATE THE CONNECTION
    clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientsocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
    print('Failed to create socket')
    sys.exit()

clientsocket1.connect(('localhost', 5001))
clientsocket2.connect(('localhost', 5002))
def sendData():
    x=1
    while x < 101:
        time.sleep(0.3)
        message = 'message '+str(x)
        print("sending "+str(message))
        if random.random() < 0.5:
            clientsocket1.send(bytes(str(x), 'UTF-8'))
            reply = clientsocket1.recv(22)
            reply = str(reply,'UTF-8')
            if not reply: break
            if reply != "DROPPED":
                print("ACK " +reply)
                x+=1
            else:
                print("DROP DETECTED")
        else:
            clientsocket2.send(bytes(str(x), 'UTF-8'))
            reply = clientsocket2.recv(22)
            reply = str(reply,'UTF-8')
            if not reply: break
            if reply != "DROPPED":
                print("ACK " +reply)
                x+=1
            else:
                print("DROP DETECTED")


sendData()
