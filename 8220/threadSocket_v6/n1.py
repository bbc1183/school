#!/usr/bin/env python

import socket
from threading import Thread
from socketserver import ThreadingMixIn
import time
import queue
import random
import sys
q = queue.Queue()

class Listener(Thread):

    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.q = queue.Queue()
        self.qmax = 15
        self.ackSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientsocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)





    def passMessage(self):
        global q
        sendStatus = False
        while sendStatus == False:
            if q.qsize() > 0:
                if q.queue[0][0] == '0':
                    if random.random() < 0.5:
                        try:
                            self.clientsocket1.connect(('localhost', 5003))

                        except:
                            #print("error connecting to node 4")
                            pass
                        sock1_sendingError = False
                        try:
                            self.clientsocket1.send(bytes(q.queue[0], 'UTF-8'))
                        except:
                            #print("error sending to node 4")
                            sock1_sendingError = True
                            pass
                        if sock1_sendingError == False:
                            sendStatus = True
                            q.get()
                    else:
                        try:
                            self.clientsocket2.connect(('localhost', 5004))
                        except:
                            #print("error connecting to node 3")
                            pass
                        sock2_sendingError = False
                        try:
                            self.clientsocket2.send(bytes(q.queue[0], 'UTF-8'))
                        except:
                            #print("error sending to node 4")
                            sock2_sendingError = True
                            pass
                        if sock2_sendingError == False:
                            sendStatus = True
                            q.get()


                #else, if message is ACK, then send back to sender
                elif q.queue[0][0] == 'A':
                    try:
                        self.ackSocket.connect(('localhost', 4999))
                    except:
                        pass
                        ackError = False
                    try:
                        self.ackSocket.send(bytes(q.queue[0], 'UTF-8'))
                    except:
                        ackError = True
                        pass
                    if ackError == False:
                        sendStatus = True
                        q.get()
            else:
                print("Queue Empty")

    def run(self):
        global q
        startTime = time.time()
        delta = .2
        #listen for incoming data
        while True:
            #print("here")

            while True:
                data = conn.recv(4)

                if data:
                    data = str(data,'UTF-8')
                    sock1_timeout = False
                    q.put(data)
                    if data[0] == '0':
                        print("N1 recieved "+data+ " Qsize: "+str(q.qsize()))
                    elif data[0] == 'A':
                        print(data)
                    #if message is meant to be forwarded
                    self.passMessage()


                #else if there is no more incoming data, empty the queue
                #print(data)
                while q.qsize() > 0:
                    self.passMessage()











TCP_IP = 'localhost'
TCP_PORT = 5001
BUFFER_SIZE = 20  # Normally 1024, but we want fast response



tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpsock.listen(4)
    #print("Waiting for incoming connections...")
    (conn, (ip,port)) = tcpsock.accept()
    conn.setblocking(1)
    thread1 = Listener(ip,port)
    #thread2 = Responder(ip,port)
    thread1.start()
    #thread2.start()
    threads.append(thread1)
    #threads.append(thread2)

for t in threads:
    t.join()
