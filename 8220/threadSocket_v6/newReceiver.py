
#!/usr/bin/env python

import socket
from threading import Thread
from socketserver import ThreadingMixIn
import time
import queue
most_curr = 0
packets = queue.Queue()

class Listener(Thread):

    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port




        #self.return_socket.connect(('localhost', 4999))

        #print("[+] New thread started for "+ip+":"+str(port))


    def run(self):
        global most_curr
        global packets
        flag = False
        while True:
            # conn_err = False
            # while conn_err == False:

            data = conn.recv(200)
            data = str(data,'UTF-8')
            orderError = False
            if data:
                packets.put(data)
                data_int = int(data)

                most_curr = data_int

                print("R recieved "+data)
            if packets.qsize == 100:
                print(packets.queue)
                break

        conn.close()

class Responder(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.response_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)






    def run(self):
        #print("in sender")
        global packets
        if packets.qsize() == 100:
            while True:
                print("packetSize: "+ str(packets.qsize()))
                if packets.qsize() > 0:
                    try:
                        self.response_socket.connect(('localhost', 4999))
                        self.response_socket.send(bytes(str(packets.queue[0]), 'UTF-8'))
                        message = 'message '+str(packets.queue[0])
                        print("SENDING: "+ message)
                    except:
                        print("error responding")

        self.response_socket.close()
        print("done")




TCP_IP = 'localhost'
TCP_PORT = 5000
BUFFER_SIZE = 20  # Normally 1024, but we want fast response



tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpsock.listen(4)
    #print("Waiting for incoming connections...")
    (conn, (ip,port)) = tcpsock.accept()
    conn.setblocking(1)
    thread1 = Listener(ip,port)
    thread2 = Responder()
    thread1.start()
    thread2.start()
    threads.append(thread1)
    threads.append(thread2)

for t in threads:
    t.join()
