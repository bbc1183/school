import socket
import random
import sys
import time

import threading



try:
    #CREATE THE CONNECTION
    clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientsocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
    print('Failed to create socket')
    sys.exit()

clientsocket1.connect(('localhost', 5001))
clientsocket2.connect(('localhost', 5002))
def sendData():
    startTime = time.time()
    droppedPackets = 0
    x=1
    while x < 71:
        time.sleep(.6)
        if x < 10:
            msg = "0"+str(x)
        else:
            msg = x
        message = 'message '+str(x)
        print("sending "+str(message))
        if random.random() < 0.5:
            clientsocket1.send(bytes(str(msg)+"/", 'UTF-8'))

        else:
            clientsocket2.send(bytes(str(msg)+"/", 'UTF-8'))
        x+=1

    print("TOTAL DROPPED PACKETS: "+str(droppedPackets))
    #after sending, then receive
    acks = []
    while True:
        clientsocket1.settimeout(.01)
        sock1_timeout = False
        sock2_timeout = False
        try:
            reply1 = clientsocket1.recv(3)
            reply1 = str(reply1,'UTF-8')
        except socket.timeout:
            #print("socket 1 timed out")
            sock1_timeout = True
            pass
        clientsocket2.settimeout(.01)
        try:
            reply2 = clientsocket2.recv(3)

            reply2 = str(reply2,'UTF-8')
        except:
            #print("socket 2 timed out")
            sock2_timeout = True
            pass
        if reply1:
            acks.append(reply1)
            #print("ACK "+ reply1)
        if reply2:
            acks.append(reply2)
            #print("ACK "+ reply2)

        if not (reply1 or reply2):
            #print("break happened here")
            break

        if sock2_timeout == True and sock1_timeout == True:
            print("both sockets timed out")
            break
    acks = set(acks)
    acks = list(acks)
    acks.sort()
    for ack in acks:
        print("ACK "+str(ack))




sendData()
