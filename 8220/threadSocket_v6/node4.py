

#!/usr/bin/env python

import socket
from threading import Thread
from socketserver import ThreadingMixIn
import time
import queue
import random

class Listener(Thread):

    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        #
        # self.clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # self.clientsocket1.connect(('localhost', 5000))
        #print("[+] New thread started for "+ip+":"+str(port))
        self.q = queue.Queue()
        self.qmax = 12



    def run(self):
        startTime = time.time()
        delta = .2
        while True:


            data = conn.recv(3)
            data = str(data,'UTF-8')

            if data:

                self.q.put(data)

                print("N4 recieved "+data+ " Qsize: "+str(self.q.qsize()))
                err = False
                try:
                    self.clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.clientsocket1.connect(('localhost', 5000))
                except:
                    err = True
                if err == False:
                    self.clientsocket1.send(bytes(self.q.queue[0], 'UTF-8'))  # echo
                    self.q.get()
            else:
                break

                
        while True:
            reply = self.clientsocket1.recv(3)

            reply = str(reply,'UTF-8')
            print("ACK "+reply)
            conn.send(bytes(str(reply), 'UTF-8'))  # echo



TCP_IP = 'localhost'
TCP_PORT = 5004
BUFFER_SIZE = 20  # Normally 1024, but we want fast response



tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpsock.listen(4)
    #print("Waiting for incoming connections...")
    (conn, (ip,port)) = tcpsock.accept()
    conn.setblocking(1)
    thread1 = Listener(ip,port)
    #thread2 = Responder(ip,port)
    thread1.start()
    #thread2.start()
    threads.append(thread1)
    #threads.append(thread2)

for t in threads:
    t.join()
