

#!/usr/bin/env python

import socket
from threading import Thread
from socketserver import ThreadingMixIn
import time
import random
import queue

most_curr = 0
q = queue.Queue()

class Listener(Thread):

    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.forward_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self.forward_socket.connect(('localhost', 5001))

        #print("[+] New thread started for "+ip+":"+str(port))
    def passMessage(self):
        global q

        if q.qsize() > 0:
            if q.queue[0][0] == '0':

                try:
                    self.clientsocket1.connect(('localhost', 5000))

                except:
                    #print("error sending to node 3")
                    pass
                try:
                    self.clientsocket1.send(bytes(q.get(), 'UTF-8'))
                except:
                    pass


            #else, if message is ACK, then send back to sender
            elif q.queue[0][0] == 'A':
                if random.random() < 0.5:
                    try:
                        self.clientsocket1.connect(('localhost', 5004))

                    except:
                        #print("error sending to node 4")
                        pass
                    try:
                        self.clientsocket1.send(bytes(q.get(), 'UTF-8'))
                    except:
                        pass


                else:
                    try:
                        self.clientsocket2.connect(('localhost', 5005))


                    except:
                        #print("error sending to node 5")
                        pass
                    try:
                        self.clientsocket2.send(bytes(q.get(), 'UTF-8'))
                    except:
                        pass
        else:
            print("queue empty")


    def run(self):
        global q

        while True:
            # conn_err = False
            # while conn_err == False:

            data = conn.recv(4)

            if data:
                data = str(data,'UTF-8')

                print("RECEIVED "+data)
                data = "A"+data[1:4]
                q.put(data)

                self.passMessage() #send ACK


TCP_IP = 'localhost'
TCP_PORT = 5000
BUFFER_SIZE = 20  # Normally 1024, but we want fast response



tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpsock.listen(4)
    #print("Waiting for incoming connections...")
    (conn, (ip,port)) = tcpsock.accept()
    conn.setblocking(1)
    thread1 = Listener(ip,port)
    thread1.start()
    threads.append(thread1)

for t in threads:
    t.join()
