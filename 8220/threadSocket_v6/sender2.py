

#!/usr/bin/env python

import socket
from threading import Thread
from socketserver import ThreadingMixIn
import time
import queue
import random

q = queue.Queue()
for x in range(1,31):
    if x < 10:

        msg = "00"+str(x)+"/"
    else:
        msg = "0"+str(x)+"/"
    print(msg)
    q.put(msg)

clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientsocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientsocket1.connect(('localhost', 5001))
clientsocket2.connect(('localhost', 5002))
def sendMessages():
    global q

    while q.qsize() > 0:
        time.sleep(.5)
        droppedPackets = 0

        print("sending "+str(q.queue[0]))
        if random.random() < 0.5:
            try:
                clientsocket1.send(bytes(str(q.get()), 'UTF-8'))
            except:
                pass
        else:
            try:
                clientsocket2.send(bytes(str(q.get()), 'UTF-8'))
            except: pass

class Listener(Thread):

    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port




    def run(self):


        while True:
            reply = conn.recv(4)
            reply = str(reply,'UTF-8')
            print(reply)


TCP_IP = 'localhost'
TCP_PORT = 4999
BUFFER_SIZE = 20  # Normally 1024, but we want fast response



tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    sendMessages()
    time.sleep(2)
    tcpsock.listen(4)
    #print("Waiting for incoming connections...")
    (conn, (ip,port)) = tcpsock.accept()
    conn.setblocking(1)
    thread1 = Listener(ip,port)
    thread1.start()
    threads.append(thread1)

for t in threads:
    t.join()
