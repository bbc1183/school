#!/usr/bin/env python

import socket
from threading import Thread
from socketserver import ThreadingMixIn
import time
import queue
import random

q = queue.Queue()

class Listener(Thread):

    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.q = queue.Queue()
        self.qmax = 15
        self.ackSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientsocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)







    def passMessage(self):
        global q
        if q.qsize() > 0:
            if q.queue[0][0] == '0':

                try:
                    self.clientsocket1.connect(('localhost', 5000))

                except:
                    #print("error sending to receiver")
                    pass
                try:
                    self.clientsocket1.send(bytes(q.queue[0], 'UTF-8'))
                    q.get()
                except: pass


            #else, if message is ACK, then send back to sender
            elif q.queue[0][0] == 'A':
                if random.random() < 0.5:
                    try:
                        self.clientsocket1.connect(('localhost', 5001))

                    except:
                        print("error sending to node 3")
                        pass
                    try:
                        self.clientsocket1.send(bytes(q.queue[0], 'UTF-8'))
                        q.get()
                    except:
                        pass


                else:
                    try:
                        self.clientsocket2.connect(('localhost', 5003))

                    except:
                        print("error sending to node 4")
                        pass
                    try:
                        self.clientsocket2.send(bytes(q.queue[0], 'UTF-8'))
                        q.get()
                    except:
                        pass
        else:
            print("Queue Empty")

    def run(self):
        startTime = time.time()
        delta = .2
        global q
        #listen for incoming data
        while True:

            data = conn.recv(8)

            if data:
                data = str(data,'UTF-8')
                sock1_timeout = False
                q.put(data)
                if data[0] == '0':
                    print("N4 recieved "+data+ " Qsize: "+str(q.qsize()))
                elif data[0] == 'A':
                    print(data)
                #if message is meant to be forwarded
                self.passMessage()

            #else if there is no more incoming data, empty the queue
            elif q.qsize() > 0:
                self.passMessage()











TCP_IP = 'localhost'
TCP_PORT = 5004
BUFFER_SIZE = 20  # Normally 1024, but we want fast response



tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpsock.listen(4)
    #print("Waiting for incoming connections...")
    (conn, (ip,port)) = tcpsock.accept()
    conn.setblocking(1)
    thread1 = Listener(ip,port)
    #thread2 = Responder(ip,port)
    thread1.start()
    #thread2.start()
    threads.append(thread1)
    #threads.append(thread2)

for t in threads:
    t.join()
