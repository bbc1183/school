

#!/usr/bin/env python

import socket
from threading import Thread
from socketserver import ThreadingMixIn
import time
import queue
import random

class Listener(Thread):

    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.q = queue.Queue()
        self.qmax = 15
        self.clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientsocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        #print("[+] New thread started for "+ip+":"+str(port))


    def run(self):
        startTime = time.time()
        delta = .2
        while True:

            data = conn.recv(3)
            data = str(data,'UTF-8')
            sock1_timeout = False
            if data:
                self.q.put(data)
                print("N1 recieved "+data+ " Qsize: "+str(self.q.qsize()))
                if random.random() < 0.5:
                    try:
                        self.clientsocket1.connect(('localhost', 5003))
                        self.clientsocket1.send(bytes(data, 'UTF-8'))

                    except:
                        print("error sending to node 3")
                        pass


                else:
                    try:
                        self.clientsocket2.connect(('localhost', 5004))
                        self.clientsocket2.send(bytes(data, 'UTF-8'))

                    except:
                        print("error sending to node 4")
                        pass



            elif self.q.qsize() > 0:
                print("no data")
                if random.random() < 0.5:
                    try:
                        self.clientsocket1.connect(('localhost', 5003))
                        self.clientsocket1.send(bytes(data, 'UTF-8'))

                    except:
                        print("error sending to node 3")
                        pass


                else:
                    try:

                        self.clientsocket2.connect(('localhost', 5004))
                        self.clientsocket2.send(bytes(data, 'UTF-8'))

                    except:
                        print("error sending to node 4")
                        pass


            elif not (data or (self.q.qsize() > 0)):
                print("break at line 121")
                break


        while True:
            print("listening")
            try:
                self.clientsocket1.connect(('localhost', 5003))
            except:
                pass
            try:
                self.clientsocket2.connect(('localhost', 5004))
            except:
                pass

            self.clientsocket1.settimeout(.01)
            sock1_timeout = False
            sock2_timeout = False
            try:
                reply1 = self.clientsocket1.recv(3)
                reply1 = str(reply1,'UTF-8')
            except socket.timeout:
                print("socket 1 timed out")
                sock1_timeout = True
                pass
            self.clientsocket2.settimeout(.01)
            try:
                reply2 = self.clientsocket2.recv(3)

                reply2 = str(reply2,'UTF-8')
            except:
                print("socket 2 timed out")
                sock2_timeout = True
                pass
            if reply1:
                conn.send(bytes(str(reply1), 'UTF-8'))  # echo
                print("ACK "+ reply1)
            if reply2:
                conn.send(bytes(str(reply2), 'UTF-8'))  # echo
                print("ACK "+ reply2)

            if not (reply1 or reply2):
                print("break happened here")
                break

            if sock2_timeout == True and sock1_timeout == True:
                print("both sockets timed out")
                break







TCP_IP = 'localhost'
TCP_PORT = 5001
BUFFER_SIZE = 20  # Normally 1024, but we want fast response



tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpsock.listen(4)
    #print("Waiting for incoming connections...")
    (conn, (ip,port)) = tcpsock.accept()
    conn.setblocking(1)
    thread1 = Listener(ip,port)
    #thread2 = Responder(ip,port)
    thread1.start()
    #thread2.start()
    threads.append(thread1)
    #threads.append(thread2)

for t in threads:
    t.join()
