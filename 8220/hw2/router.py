import socket
import sys
import time
import random

"""
GLOBAL VARS
"""
drop_prb = 19


router_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
router_socket.bind(('localhost', 8089))
router_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
router_socket.listen(5) # become a server socket, maximum 5 connections

while True:
    #WHILE TRUE, ACCEPT CONNECTIONS FROM SENDER
    connection, address = router_socket.accept()
    buf = connection.recv(64)
    #router_socket.close()
    if len(buf) > 0:
        message_from_sender=str(buf,'utf-8')

        #print(message_from_sender)
        routerMSG = "router received "+message_from_sender
        print(routerMSG)


        """
        -------------------------------------------------------------------------
        BLOCK_START:  THIS BLOCK ESTABLISHES THE CONNECTION TO THE RECEIVER AND
            RECEIVES A REPLY
            - WE WILL DROP PACKETS WITH PROBABILITY = drop.prb
            - WE WILL DELAY PACKETS WITH .001 <= DELAY <= .01
        -------------------------------------------------------------------------
        """

        try:
            router2receiver_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error:
            print('Failed to create socket')
            sys.exit()

        router2receiver_socket.connect(('localhost', 5000))

        try:
            time.sleep(random.uniform(.001,.01)) #delay packet for random amount of time
            #with probability 19% drop packet, that is: do not send
            if random.random()*100 > drop_prb:
                router2receiver_socket.send(bytes(message_from_sender, 'UTF-8'))
                #router2receiver_socket.close()
            else:
                print("router dropped "+message_from_sender)
                #router2receiver_socket.send(bytes(' ', 'UTF-8'))

                reply = router2receiver_socket.recv(4096)
                reply = str(reply,'UTF-8')
                #print("the reply: "+str(reply))
                #this is the message we need to send back to sender
                connection.sendall(bytes(reply, 'UTF-8'))
                router2receiver_socket.close()



        except socket.error:
            print('router Send failed')
            sys.exit()


        """
        -------------------------------------------------------------------------
        END BLOCK
        -------------------------------------------------------------------------
        """
        #print(reply)
