import socket
import random
import sys
import time


"""
GLOBAL VARS
"""
Y = 100


tries_y1 = 0
tries_y2 = 0
tries_y3= 0
tries_y4 = 0
tries_y5 = 0
tries_y6 = 0
startTime = time.time()
msg_idx = 0
currMSG = 1
while msg_idx < Y:
    messageSuccess = 0
    while messageSuccess == 0:
    #     if idx == 0:
    #         tries_y1 +=1
    #     if idx == 1:
    #         tries_y2 +=1
    #     if idx == 2:
    #         tries_y3 +=1
    #     if idx == 3:
    #         tries_y4 +=1
    #     if idx == 4:
    #         tries_y5 +=1
    #     if idx == 5:
    #         tries_y6 +=1
        """
        -------------------------------------------------------------------------
        BLOCK_START:  THIS BLOCK ESTABLISHES THE CONNECTION TO THE ROUTER AND
            RECEIVES A REPLY.  ADDITIONALLY, IT IS RESPONSIBLE FOR GENERATING
            MESSAGES TO SEND, AND ALSO FOR DETERMINING WHICH MESSAGES TO RE-SEND
            IN THE EVENT OF A NEGATIVE RESPONSE FROM THE ROUTER, HENCE THE NAME
            NTCP.
        -------------------------------------------------------------------------
        """
        try:
            clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error:
            print('Failed to create socket')
            sys.exit()

        clientsocket.connect(('localhost', 8089))

        message = 'message: '+str(currMSG)
        try:
            print("sending "+message)
            #startTime = time.time()
            clientsocket.send(bytes(message, 'UTF-8'))

        except socket.error:
            print('Send failed')
            sys.exit()

        clientsocket.settimeout(0.02)
        #listen for a reply of dropped
        try:
            reply = clientsocket.recv(4096)

            reply = str(reply,'UTF-8')
            status = reply.split(' ')[0]
            #print(status)
            if status == 'detected':
                print(reply)
                print("RE-TRANSMITTING: "+message)

        except socket.timeout:
            #print(reply)
            messageSuccess = 1
            currMSG += 1
            msg_idx += 1

print(time.time() - startTime)
print(msg_idx)






"""
-------------------------------------------------------------------------
END BLOCK
-------------------------------------------------------------------------
"""
