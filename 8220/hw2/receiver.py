import sys
import socket
import time

recv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
recv_socket.bind(('localhost',5000))
recv_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
recv_socket.listen(5) # become a server socket, maximum 5 connections

latest_received_message = 0
curr_message_idx = 0
while True:

    #WHILE TRUE, ACCEPT CONNECTIONS FROM ROUTER
    connection, address = recv_socket.accept()
    connection.settimeout(0.01)
    try:
        #listen for a second
        buf = connection.recv(64)
        #if we find a message in the buffer, proceed
        if len(buf) > 0:
            #print(buf)

            message_from_router=str(buf,'utf-8')
            #print(message_from_router)

            if(len(message_from_router) > 1):

                curr_message_idx = int(message_from_router.split(": ")[1])
                latest_received_message = curr_message_idx
                receiverMSG = "received "+message_from_router
                print(receiverMSG)

                #connection.sendall(bytes(receiverMSG, 'UTF-8'))
            else:
                #print("msgIDX"+str(curr_message_idx))
                if curr_message_idx != (latest_received_message+1):
                    receiverMSG = "dropped "+message_from_router
                    print(receiverMSG)
                    connection.sendall(bytes(receiverMSG, 'UTF-8'))
    #if we timeout, then it seems the message has been dropped
    except socket.timeout:
        #print("msgIDX"+str(curr_message_idx))
        if curr_message_idx != (latest_received_message+1):
            receiverMSG = "detected dropped message"
            print(receiverMSG)
            connection.sendall(bytes(receiverMSG, 'UTF-8'))
