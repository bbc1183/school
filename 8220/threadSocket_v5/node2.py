

#!/usr/bin/env python

import socket
from threading import Thread
from socketserver import ThreadingMixIn
import time
import queue
import random

class Listener(Thread):

    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port

        self.clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientsocket1.connect(('localhost', 5000))
        print("[+] New thread started for "+ip+":"+str(port))
        self.q = queue.Queue()
        self.qmax = 12



    def run(self):
        startTime = time.time()
        delta = .4
        while True:


            data = conn.recv(200)
            data = str(data,'UTF-8')

            if data:
                if self.q.qsize() < self.qmax:
                    self.q.put(data)
                    data_int = int(data)
                    print("N2 recieved "+data+ " Qsize: "+str(self.q.qsize()))
                    conn.send(bytes(str(data), 'UTF-8'))  # echo
                    if time.time() > startTime+delta:

                        err = False
                        try:
                            self.clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                            self.clientsocket1.connect(('localhost', 5000))
                        except:
                            err = True
                        if err == False:
                            self.clientsocket1.send(bytes(self.q.queue[0], 'UTF-8'))  # echo

                            reply = self.clientsocket1.recv(22)

                            reply = str(reply,'UTF-8')
                            if reply:
                                if reply != "OOO":
                                    print("ACK " +str(reply))
                                    self.q.get()
                                    startTime = time.time()
                                else:
                                    startTime = time.time()
                else:
                    conn.send(bytes(str("DROPPED"), 'UTF-8'))  # echo
                    if time.time() > startTime+delta:

                        err = False
                        try:
                            self.clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                            self.clientsocket1.connect(('localhost', 5000))
                        except:
                            err = True
                        if err == False:
                            self.clientsocket1.send(bytes(self.q.queue[0], 'UTF-8'))  # echo

                            reply = self.clientsocket1.recv(22)

                            reply = str(reply,'UTF-8')
                            if reply:
                                if reply != "OOO":
                                    print("ACK " +str(reply))
                                    self.q.get()
                                    startTime = time.time()
                                else:
                                    startTime = time.time()
            elif self.q.qsize() > 0:
                if time.time() > startTime+delta:

                    err = False
                    try:
                        self.clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        self.clientsocket1.connect(('localhost', 5000))
                    except:
                        err = True
                    if err == False:
                        self.clientsocket1.send(bytes(self.q.queue[0], 'UTF-8'))  # echo

                        reply = self.clientsocket1.recv(22)

                        reply = str(reply,'UTF-8')
                        if reply:
                            if reply != "OOO":
                                print("ACK " +str(reply))
                                self.q.get()
                                startTime = time.time()
                            else:
                                startTime = time.time()



TCP_IP = 'localhost'
TCP_PORT = 5002
BUFFER_SIZE = 20  # Normally 1024, but we want fast response



tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpsock.listen(4)
    print("Waiting for incoming connections...")
    (conn, (ip,port)) = tcpsock.accept()
    conn.setblocking(1)
    thread1 = Listener(ip,port)
    #thread2 = Responder(ip,port)
    thread1.start()
    #thread2.start()
    threads.append(thread1)
    #threads.append(thread2)

for t in threads:
    t.join()
