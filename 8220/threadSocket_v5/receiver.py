

#!/usr/bin/env python

import socket
from threading import Thread
from socketserver import ThreadingMixIn
import time
import queue
import threading




class Forwarder(Thread):

    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.q = queue.Queue()
        print("[+] New thread started for "+ip+":"+str(port))



    def run(self):

        while True:
            data = conn.recv(2048)
            data = str(data,'UTF-8')
            print("forwarding: "+data)
            self.sendingQ.put(data)

            self.clientsocket1.send(bytes(self.sendingQ.get(), 'UTF-8'))  # echo
            reply= self.clientsocket1.recv(200)
            reply = str(reply,'UTF-8')
            print("responses received: "+reply)
            conn.send(bytes(reply, 'UTF-8'))



class Responder(Thread):
    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientsocket1.connect(('localhost', 5001))
        self.sendingQ = queue.Queue()
        self.responseQ = queue.Queue()


        print("[+] New thread started for "+ip+":"+str(port))



    def run(self):

        while True:
            data = conn.recv(2048)
            data = str(data,'UTF-8')
            print("forwarding: "+data)
            self.sendingQ.put(data)

            self.clientsocket1.send(bytes(self.sendingQ.get(), 'UTF-8'))  # echo
            reply= self.clientsocket1.recv(200)
            reply = str(reply,'UTF-8')
            print("responses received: "+reply)
            conn.send(bytes(reply, 'UTF-8'))



TCP_IP = 'localhost'
TCP_PORT = 5000
BUFFER_SIZE = 20  # Normally 1024, but we want fast response




tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientsocket1.connect(('localhost', 5001))
q = queue.Queue()

while True:
    tcpsock.listen(4)
    print("Waiting for incoming connections...")
    (conn, (ip,port)) = tcpsock.accept()
    conn.setblocking(1)
    thread1 = Forwarder(ip,port)
    thread2 = Responder(ip,port)
    thread1.start()
    thread2.start()
    threads.append(thread1)

for t in threads:
    t.join()
