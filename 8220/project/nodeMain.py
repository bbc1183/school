import _thread as thread
import node1
import node2
import node3
import node4
import node5
import node6
import node7
import node8


try:
    thread.start_new_thread(node1.r1.work())
    thread.start_new_thread(node2.r2.work())
    thread.start_new_thread(node3.r3.work())
    thread.start_new_thread(node4.r4.work())
    thread.start_new_thread(node5.r5.work())
    thread.start_new_thread(node6.r6.work())
    thread.start_new_thread(node7.r7.work())
    thread.start_new_thread(node8.r8.work())
except:
    print("Error: unable to start thread")
