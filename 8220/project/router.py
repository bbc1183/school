import socket
import sys
import time
import random
import queue


"""
GLOBAL VARS
"""


"""
SENDER:
    PORT1: 8089
    PORT2: 8090


NODES:
    NODE1:
        INPORT: 8089
        OUTPORT_1: 5000
        OUTPORT_2: 5001
    NODE2:
        INPORT: 8090
        OUTPORT_1: 5000
        OUTPORT_2: 5002
    NODE3:
        INPORT: 5000
        OUTPORT_1: 5001
        OUTPORT_2: 5002
    NODE4:
        INPORT: 5001
        OUTPORT_1: 5003
        OUTPORT_2: 5004
    NODE5:
        INPORT: 5002
        OUTPORT_1: 5003
        OUTPORT_2: 5005
    NODE6:
        INPORT: 5003
        OUTPORT_1: 5004
        OUTPORT_2: 5005
    NODE7:
        INPORT: 5004
        OUTPORT_1: 5006

    NODE8:
        INPORT: 5005
        OUTPORT_1: 5006

RECEIVER:
    PORT1: 5006

"""
drop_prb = 19


class router(object):
    def __init__(self, id, maxQsize, inport, outport1,outport2=None):
        self.id = id
        self.maxQsize = maxQsize
        self.inport = inport
        self.outport1 = outport1
        self.outport2 = outport2
        self.router_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.router_socket.bind(('localhost', self.inport))
        self.router_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.router_socket.listen(2) # become a server socket, maximum 5 connections

        self.q = queue.Queue(maxsize= self.maxQsize)

    def work(self):

        while True:
            line_rate_start = time.time()
            #keep receiving messages for a time before trying to forward the message
            #this is how we force the queue to begin filling up
            #while(time.time() < line_rate_start+10):
            #WHILE TRUE, ACCEPT CONNECTIONS FROM SENDER
            sender2router, address = self.router_socket.accept()
            buf = sender2router.recv(64)
            #router_socket.close()
            if len(buf) > 0:
                message_from_sender=str(buf,'utf-8')

                #print(message_from_sender)
                routerMSG = "Node "+str(self.id)+" received "+message_from_sender
                print(routerMSG)
                #print("Size of Queue: "+ str(self.q.qsize()))
                self.q.put(message_from_sender)
                #print("Size of Queue: "+ str(self.q.qsize()))
                #print(self.q.queue)

                """
                -------------------------------------------------------------------------
                BLOCK_START:  THIS BLOCK ESTABLISHES THE CONNECTION TO THE RECEIVER AND
                    RECEIVES A REPLY

                -------------------------------------------------------------------------
                """

            print("size of queue: "+ str(self.q.qsize()))
            if self.id != 7 and self.id != 8:
                #print("/")
                #THIS IS WHERE WE SEND A MESSAGE

                try:
                    #CREATE THE CONNECTION
                    Node_clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    Node_clientsocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                except socket.error:
                    print('Failed to create socket')
                    #sys.exit()

                #CONNECT TO THE SOCKET
                Node_clientsocket1.connect(('localhost', self.outport1))
                Node_clientsocket2.connect(('localhost', self.outport2))

                #print("/")

                if(self.q.qsize() > 0):
                    try:
                        #print("trying to send")
                        if random.random() > 0.5:
                            print("Now Sending "+self.q.queue[0]+" to Port: "+str(self.outport1))
                            Node_clientsocket1.send(bytes(self.q.queue[0], 'UTF-8'))  #send the first item in queue, but dont remove until receipt confirmation
                            #listen briefly for reply
                            Node_clientsocket1.settimeout(0.2)
                            try:

                                reply = Node_clientsocket1.recv(100)
                                reply = str(reply,'UTF-8')
                                print(str(reply))
                                currMSG = self.q.get()  #remove from queue here because we have receipt confirmation
                                #this is the message we need to send back to sender
                                sender2router.sendall(bytes(reply, 'UTF-8'))
                                sender2router.close()

                            except socket.timeout:
                                #print("socket timed out, should not remove q element")
                                continue
                        else:
                            print("Now Sending "+self.q.queue[0]+" to Port: "+str(self.outport2))
                            Node_clientsocket2.send(bytes(self.q.queue[0], 'UTF-8'))

                            #listen briefly for reply
                            Node_clientsocket2.settimeout(0.2)
                            try:
                                reply = Node_clientsocket2.recv(100)
                                reply = str(reply,'UTF-8')
                                print(str(reply))
                                currMSG = self.q.get()  #remove from queue here because we have receipt confirmation
                                #this is the message we need to send back to sender
                                sender2router.sendall(bytes(reply, 'UTF-8'))
                                sender2router.close()

                            except socket.timeout:
                                #print("socket timed out, should not remove q element")
                                continue

                    except socket.error:
                        print('router Send failed')


            else:
                #print("last node outport: "+str(self.outport1))
                #when the line rate limit has been reached, try to forward message
                try:
                    router2receiver_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                except socket.error:
                    print('Failed to create socket')
                    #sys.exit()

                router2receiver_socket.connect(('localhost', self.outport1))

                if(self.q.qsize() > 0):
                    try:
                        #print("trying to send")
                        print("Now Sending "+self.q.queue[0]+" to Receiver")
                        router2receiver_socket.send(bytes(self.q.queue[0], 'UTF-8'))  #send the first item in queue, but dont remove until receipt confirmation



                        #listen briefly for reply
                        router2receiver_socket.settimeout(0.002)
                        try:
                            reply = router2receiver_socket.recv(100)
                            reply = str(reply,'UTF-8')
                            print(str(reply))
                            currMSG = self.q.get()  #remove from queue here because we have receipt confirmation
                            #this is the message we need to send back to sender
                            sender2router.sendall(bytes(reply, 'UTF-8'))
                            sender2router.close()

                        except socket.timeout:
                            #print("socket timed out, should not remove q element")
                            continue

                        #GET REPLY FROM RECEIVER, WILL BE ACK

                    except socket.error:
                        print('router Send failed')










                """
                -------------------------------------------------------------------------
                END BLOCK
                -------------------------------------------------------------------------
                """
                #print(reply)

"""
STILL NEED TO SCALE UP NODES AND THEN ADDRESS DROPPED PACKETS
"""
