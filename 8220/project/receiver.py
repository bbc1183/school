import sys
import socket
import time

recv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
recv_socket.bind(('localhost',5006))
recv_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
recv_socket.listen(2) # become a server socket, maximum 5 connections

latest_received_message = 0
curr_message_idx = 0
while True:

    #WHILE TRUE, ACCEPT CONNECTIONS FROM ROUTER
    router2receiver, address = recv_socket.accept()
    router2receiver.settimeout(0.1)
    try:
        #listen for a second
        buf = router2receiver.recv(64)
        #if we find a message in the buffer, proceed
        if len(buf) > 0:
            #print(buf)

            message_from_router=str(buf,'utf-8')
            #print(message_from_router)

            if(len(message_from_router) > 1):

                curr_message_idx = int(message_from_router.split(": ")[1])
                latest_received_message = curr_message_idx
                receiverMSG = "Receiver received:  message "+str(curr_message_idx)
                print(receiverMSG)
                router2receiver.sendall(bytes(receiverMSG, 'UTF-8'))

                #connection.sendall(bytes(receiverMSG, 'UTF-8'))
            else:
                #print("msgIDX"+str(curr_message_idx))
                if curr_message_idx != (latest_received_message+1):
                    receiverMSG = "dropped "+message_from_router
                    print(receiverMSG)
                else:
                    print("something else wrong")


    #if we timeout, then it seems the message has been dropped
    except socket.timeout:
        #print("msgIDX"+str(curr_message_idx))
        if curr_message_idx != (latest_received_message+1):
            receiverMSG = "reciever timed out"
            print(receiverMSG)
            router2receiver.sendall(bytes(receiverMSG, 'UTF-8'))
