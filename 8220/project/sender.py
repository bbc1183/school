import socket
import random
import sys
import time


"""
GLOBAL VARS
"""
Y = 100


tries_y1 = 0
tries_y2 = 0
tries_y3= 0
tries_y4 = 0
tries_y5 = 0
tries_y6 = 0
startTime = time.time()
msg_idx = 0
currMSG = 1
while msg_idx < Y:
    messageSuccess = 0
    while messageSuccess == 0:
    #     if idx == 0:
    #         tries_y1 +=1
    #     if idx == 1:
    #         tries_y2 +=1
    #     if idx == 2:
    #         tries_y3 +=1
    #     if idx == 3:
    #         tries_y4 +=1
    #     if idx == 4:
    #         tries_y5 +=1
    #     if idx == 5:
    #         tries_y6 +=1
        """
        -------------------------------------------------------------------------
        BLOCK_START:  THIS BLOCK ESTABLISHES THE CONNECTION TO THE ROUTER AND
            RECEIVES A REPLY.  ADDITIONALLY, IT IS RESPONSIBLE FOR GENERATING
            MESSAGES TO SEND, AND ALSO FOR DETERMINING WHICH MESSAGES TO RE-SEND
            IN THE EVENT OF A NEGATIVE RESPONSE FROM THE ROUTER, HENCE THE NAME
            NTCP.
        -------------------------------------------------------------------------
        """
        #THIS IS WHERE WE SEND A MESSAGE
        try:
            #CREATE THE CONNECTION
            clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            clientsocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error:
            print('Failed to create socket')
            sys.exit()

        #CONNECT TO THE SOCKET
        clientsocket1.connect(('localhost', 8089))
        clientsocket2.connect(('localhost', 8090))

        #CONSTRUCT MESSAGE
        message = 'message: '+str(currMSG)
        time.sleep(random.randint(1,100)*.001) #this is how we affect the line rate

        try:
            #startTime = time.time()
            #SEND THE MESSAGE TO EITHER NODE 1 OR NODE 2
            if random.random() > 0.5:
                print("sending port 1 "+message)
                clientsocket1.send(bytes(message, 'UTF-8'))

                #wait briefly for reply
                clientsocket1.settimeout(0.002)

                #THIS IS WHERE WE LISTEN FOR A REPLY
                try:
                    #print("sender line 82")
                    reply1 = clientsocket1.recv(100)

                    #print("sender line 85")

                    reply1 = str(reply1,'UTF-8')

                    status1 = reply1.split(' ')[0]

                    #print(status)
                    # if status == 'detected':
                    print(reply1)

                except socket.timeout:
                    print("timed out")
                    r = 0

            else:
                print("sending port 2 "+message)
                clientsocket2.send(bytes(message, 'UTF-8'))
                #wait briefly for a reply
                clientsocket2.settimeout(0.3)
                #THIS IS WHERE WE LISTEN FOR A REPLY
                try:
                    #print("socket2start")
                    reply2 = clientsocket2.recv(100)
                    #print("socket2 end")
                    reply2 = str(reply2,'UTF-8')
                    status2 = reply2.split(' ')[0]
                    #print(status)
                    # if status == 'detected':
                    print(reply2)
                except socket.timeout:
                    #print("timed out")
                    r = 0

        except socket.error:
            print('Send failed')

        messageSuccess = 1
        currMSG += 1
        msg_idx += 1




"""
-------------------------------------------------------------------------
END BLOCK
-------------------------------------------------------------------------
"""
