import socket
import random
import sys
import time

import threading



try:
    #CREATE THE CONNECTION
    clientsocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientsocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
    print('Failed to create socket')
    sys.exit()

clientsocket1.connect(('localhost', 5001))
clientsocket2.connect(('localhost', 5002))
def sendData():
    x=1
    while x < 101:
        time.sleep(0.3)
        message = 'message '+str(x)
        print("sending "+str(message))
        if random.random() < 0.5:
            clientsocket1.send(bytes(str(x), 'UTF-8'))
            reply = clientsocket1.recv(22)
            reply = str(reply,'UTF-8')
            if not reply: break
            print("ACK " +reply)
            x+=1
        else:
            clientsocket2.send(bytes(str(x), 'UTF-8'))
            reply = clientsocket2.recv(22)
            reply = str(reply,'UTF-8')
            if not reply: break
            print("ACK " +reply)
            x+=1


sendData()
# threads = []
# t1 = threading.Thread(target=sendData)
# t2 = threading.Thread(target=listen)
#
# t1.start()
# t2.start()
# threads.append(t1)
# threads.append(t2)
# for t in threads:
#     t.join()
