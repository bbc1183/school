

#!/usr/bin/env python

import socket
from threading import Thread
from socketserver import ThreadingMixIn
import time

most_curr = 0

class Listener(Thread):

    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.forward_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self.forward_socket.connect(('localhost', 5001))

        #print("[+] New thread started for "+ip+":"+str(port))


    def run(self):
        global most_curr

        while True:
            # conn_err = False
            # while conn_err == False:

            data = conn.recv(200)
            data = str(data,'UTF-8')
            orderError = False
            if data:
                data_int = int(data)
                if data_int == most_curr+1:
                    most_curr = data_int

                    conn.send(bytes(data, 'UTF-8'))  # echo
                    print("R recieved "+data)
                else:
                    conn.send(bytes("OOO", 'UTF-8'))
                    #print("out of order, resending")

        conn.close()














TCP_IP = 'localhost'
TCP_PORT = 5000
BUFFER_SIZE = 20  # Normally 1024, but we want fast response



tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpsock.listen(4)
    #print("Waiting for incoming connections...")
    (conn, (ip,port)) = tcpsock.accept()
    conn.setblocking(1)
    thread1 = Listener(ip,port)
    thread1.start()
    threads.append(thread1)

for t in threads:
    t.join()
